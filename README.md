# findcruft2


Cruft detection tool for Gentoo


- This is my branch with additional false positives coming from some real gentoo systems (64bit) and some virtual gentoo system (32bit/64bit).

- Original hollow branch (https://github.com/hollow/findcruft2 - no longer exists) produced >50kb of cruft for each system in my environment so I added several additional package files containing addtional false positives.


### For Gentoo Linux
Tool can be found on amino overlay (https://gitlab.com/idi1806/amino)

### History
2018-06-11 moved to gitlab.com
